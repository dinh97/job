// Job
.job-item {
    padding: 30px 0 30px 0;
    color: #777;
    position: relative;
    a {
        color: #777;
        &:hover {
            color: #2980b9;
        }
    }
    .job-image {
        border-radius: 5px;
        float: left;
        margin-top: 7px;
        overflow: hidden;
        max-width: 45px;
    }
    .job-info {
        margin-left: 80px;
        padding-right: 145px;
        position: relative;
        line-height: 28px;
        i {
            width: 15px;
            font-size: 13px;
            text-align: center;
            margin-right: 10px;
        }
        .sallary i {
            margin-right: 13px;
            color: #4a98ae;
        }
        .address i {
            color: #f16e8e;
            font-size: 14px;
        }
        .job-type {
            position: absolute;
            right: 0;
            top: 0;
            &.full-time a.type-name {
                color: #dd9933;
                &:hover {
                    color: #fff;
                    background-color: #dd9933;
                }
            }
            &.contract a.type-name {
                color: #dd3333;
                &:hover {
                    color: #fff;
                    background-color: #dd3333;
                }
            }
            &.temporary a.type-name {
                color: #8224e3;
                &:hover {
                    color: #fff;
                    background-color: #8224e3;
                }
            }
            &.part-time a.type-name {
                color: #1e73be;
                &:hover {
                    color: #fff;
                    background-color: #1e73be;
                }
            }
            a.type-name {
                display: inline-block;
                color: #4a98ae;
                font-weight: 600;
                height: 22px;
                line-height: 20px;
                padding: 0 15px;
                border: 1px #f6f7f9 solid;
                border-radius: 30px;
                font-size: 10px;
                text-transform: uppercase;
                transition: all 0.3s;
            }
            button.save-job {
                background: none;
                border: none;
                padding: 0;
                i {
                    margin: 0;
                    display: inline-block;
                    height: 20px;
                    line-height: 20px;
                    width: 20px;
                    text-align: center;
                    color: #f16e8e;
                    border: 1px #f6f7f9 solid;
                    border-radius: 50%;
                    font-size: 9px;
                    margin-left: 5px;
                    transition: all 0.3s;
                    &:hover {
                        background: #f16e8e;
                        color: #fff;
                    }
                }
            }
        }
    }
    .job-title {
        margin: 0 0 10px;
        color: #333;
        a {
            color: #333333;
            font-weight: 600;
            text-transform: uppercase;
            font-size: 13px;
            display: inline-block;
            &:hover {
                color: #2980b9;
            }
        }
    }
    .au-feature {
        position: absolute;
        top: 0;
        left: 0;
        width: 38px;
        height: 38px;
        background: url(../images/job-featured.png) no-repeat;
        background-size: cover;
    }
    &.feature-item {
        .job-title {
            color: #f16e8e;
            a {
                color: #f16e8e;
            }
        }
    }
}

.au-jobs-carousel {
    border-bottom: none;
    position: relative;
    .au-items {
        padding: 15px;
        .au-item {
            border: none !important;
            margin-bottom: 30px;
            position: relative;
            z-index: 0;
            &:after {
                position: absolute;
                content: "";
                width: calc(100% - 30px);
                height: 100%;
                top: 0;
                left: 15px;
                border-radius: 5px;
                background-color: #ffffff;
                box-shadow: 0 5px 25px 0 rgba(41, 128, 185, 0.15);
                z-index: -1;
            }
        }
    }
    .job-item {
        padding: 20px 30px;
    }
    .owl-dots {
        counter-reset: dots;
        text-align: center;
        .owl-dot {
            border-radius: 5px;
            background-color: #ffffff;
            box-shadow: 0 3px 7px 0 rgba(41, 128, 185, 0.15);
            color: #333333;
            font-size: 13px;
            font-weight: 600;
            display: inline-block;
            width: 35px;
            height: 35px;
            text-align: center;
            line-height: 30px;
            margin: 3px;
            opacity: 1;
            position: relative;
            &:hover {
                background-color: #2980b9;
                color: #fff;
                transition: all 0.3s;
            }
            &:before {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                counter-increment: dots;
                content: counter(dots);
            }
            &.active {
                background-color: #2980b9;
                color: #fff;
            }
        }
    }
}

@media (max-width: 1024px) {
    .job-item .job-info {
        padding-right: 0;
        margin-left: 65px;
        .job-type {
            position: relative;
            margin-top: 5px;
        }
    }
}