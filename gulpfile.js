'use strict';

const page = ['index', 'shopping-cart'];

// Define Gulp Plugins

const   gulp = require('gulp'),
    del = require('del'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),
    fileSystem = require('fs'),
    gulpSequence = require('gulp-sequence'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename'),
    include = require("gulp-include"),
    googleWebFonts = require('gulp-google-webfonts'),

    pug = require('gulp-pug'),
    yaml = require('gulp-yaml'),
    mergeJson = require('gulp-merge-json'),
    w3cjs = require('gulp-w3cjs'),
    html_prettify = require('gulp-html-prettify'),
    html_minify = require('gulp-minify-html'),

    sass = require('gulp-sass'),
    sassGlob = require('gulp-sass-glob'),
    css_minify = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer'),

    minifyJs = require('gulp-minify');

// _______________________________SETUP_______________________________________


// Brower sync
gulp.task('browser-sync', function () {
    return browserSync.init({
        server: {
            baseDir: 'dist'
        },
        open: false
    });
});

// Clean
gulp.task('clean', function () {
    return del(['dist']);
});

gulp.task('clean-temp', function () {
    return del(['production/compress/.temp', 'production/compress/js/global.js', 'production/compress/js/global.min.js']);
});

gulp.task('clean-production', function () {
    return del(['production']);
});

// _______________________________VENDOR______________________________________

gulp.task('vendor', function () {
    gulp.src('app/vendor/*/**')
        .pipe(gulp.dest('dist/vendor'));
});

gulp.task('vendor-special', function () {
    gulp.src('app/vendor-special/*/**')
        .pipe(gulp.dest('dist/vendor'));
});

gulp.task('vendor-export', function () {
    gulp.src(['app/vendor/*/**'])
        .pipe(gulp.dest('production/release/vendor'))
        .pipe(gulp.dest('production/compress/.temp/vendor'));
});

gulp.task('vendor-special-export', function () {
    gulp.src(['app/vendor-special/*/**'])
        .pipe(gulp.dest('production/release/vendor'))
        .pipe(gulp.dest('production/compress/vendor'));
});

// _______________________________BUILD HTML___________________________________

const views_options = {
    prettify: {
        'indent_size': 4,
        'unformatted': ['pre', 'code'],
        'indent_with_tabs': false,
        'preserve_newlines': true,
        'brace_style': 'expand',
        'end_with_newline': true,
        'indent_char': ' ',
        'space_before_conditional': true,
        'wrap_attributes': 'auto'
    }
};

gulp.task('yaml-json', function () {
    return gulp.src(['app/**/*.yml'])
        .pipe(plumber())
        .pipe(yaml({ schema: 'DEFAULT_SAFE_SCHEMA' }))
        .pipe(mergeJson({
            fileName: 'data.json',
            json5: false
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('.tmp'));
});

gulp.task('views', function () {
    const data = JSON.parse(fileSystem.readFileSync('.tmp/data.json'));
    return gulp.src('app/*.pug')
        .pipe(plumber())
        .pipe(pug({pretty: true, locals: data}))
        .pipe(w3cjs())
        .pipe(w3cjs.reporter())
        .pipe(html_prettify(views_options.prettify))
        .pipe(plumber.stop())
        .pipe(gulp.dest('dist'));
});

gulp.task('views-only', function (done) {
    const data = JSON.parse(fileSystem.readFileSync('.tmp/data.json'));
    const pathPage = [];
    for (var i = 0; i < page.length; i++) {
        pathPage.push("app/" + page[i] + '.pug');
    }
    console.log(pathPage);
    gulp.src(pathPage)
        .pipe(plumber())
        .pipe(pug({pretty: true, locals: data}))
        .pipe(html_prettify(views_options.prettify))
        .pipe(plumber.stop())
        .pipe(gulp.dest('dist'))
        .on('end', done)
});

gulp.task('build-html', function (cb) {
    return gulp.src('app/*.html')
        // .pipe(plumber())
        // .pipe(w3cjs())
        // .pipe(w3cjs.reporter())
        // // .pipe(html_prettify(views_options.prettify))
        // .pipe(plumber.stop())
        .pipe(gulp.dest('dist'));
});

gulp.task('build-html-only', function (cb) {
    return gulpSequence(
        'yaml-json',
        'views-only',
        cb
    );
});

gulp.task('views-export-1', function () {
    const data = JSON.parse(fileSystem.readFileSync('.tmp/data.json'));
    return gulp.src(['app/*.pug', '!app/sample.pug'])
        .pipe(pug({pretty: true, locals: data}))
        .pipe(html_prettify(views_options.prettify))
        .pipe(gulp.dest('production/release'));
});

gulp.task('views-export-2', function () {
    const data = JSON.parse(fileSystem.readFileSync('.tmp/data.json'));
    return gulp.src(['app/*.pug', '!app/sample.pug'])
        .pipe(replace('views/master', 'views/master-compress'))
        .pipe(pug({pretty: true, locals: data}))
        .pipe(html_prettify(views_options.prettify))
        .pipe(gulp.dest('production/compress'));
});

gulp.task('build-html-export', function (cb) {
    return gulpSequence(
        'yaml-json',
        'views-export-1',
        'views-export-2',
        cb
    );
});

// _______________________________BUILD CSS_________________________________

const AUTOPREFIXER_BROWSERS = [
    'ie >= 1',
    'ie_mob >= 1',
    'ff >= 1',
    'chrome >= 1',
    'safari >= 1',
    'opera >= 1',
    'ios >= 1',
    'android >= 1',
    'bb >= 1'
];

gulp.task('css-build', function () {
    return gulp
        .src('app/scss/*.scss')
        .pipe(plumber())
        .pipe(sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['.']
        }).on('error', sass.logError))
        .pipe(plumber.stop())
        .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
        }))
        .pipe(gulp.dest('dist/css'))
        .pipe(css_minify())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('dist/css'))
        .pipe(reload({stream: true}));
});

gulp.task('css-export', function () {
    return gulp
        .src('app/css/*.scss')
        .pipe(sassGlob())
        .pipe(sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['.']
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
        }))
        .pipe(gulp.dest('production/release/css'))
        .pipe(css_minify())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('production/release/css'))
        .pipe(gulp.dest('production/compress/.temp/css'));
});

gulp.task("css-colors-compress", function() {
    gulp.src("app/css/compressed.css")
        .pipe(include({
            hardFail: true,
            includePaths: [
                "production/compress/.temp"
            ]
        }))
        .pipe(css_minify())
        .pipe(gulp.dest("production/compress/css"));
});

// Color CSS

gulp.task('colors-build', function () {
    return gulp
        .src('app/css/colors/*.scss')
        .pipe(plumber())
        .pipe(sassGlob())
        .pipe(sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['.']
        }).on('error', sass.logError))
        .pipe(plumber.stop())
        .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
        }))
        .pipe(gulp.dest('dist/css/colors'))
        .pipe(css_minify())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('dist/css/colors'))
        .pipe(reload({stream: true}));
});

gulp.task('colors-export', function () {
    return gulp
        .src('app/css/colors/*.scss')
        .pipe(sassGlob())
        .pipe(sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['.']
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
        }))
        .pipe(gulp.dest('production/release/css/colors'))
        .pipe(gulp.dest('production/compress/css/colors'))
        .pipe(css_minify())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('production/release/css/colors'))
        .pipe(gulp.dest('production/compress/css/colors'));
});

gulp.task('build-css-export', function (cb) {
    return gulpSequence(
        'css-export',
        'colors-export',
        'css-colors-compress',
        cb
    );
});

// _______________________________BUILD JS_________________________________

gulp.task('js-build', function () {
    return gulp.src(['app/js/**/*.js', '!app/js/compressed.js'])
        .pipe(plumber())
        .pipe(minifyJs({
            ext:{
                src:'.js',
                min:'.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-export', function () {
    return gulp.src(['app/js/**/*.js', '!app/js/compressed.js'])
        .pipe(minifyJs({
            ext:{
                src:'.js',
                min:'.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest('production/release/js'))
        .pipe(gulp.dest('production/compress/.temp/js'))
        .pipe(gulp.dest('production/compress/js'));
});

gulp.task("compress-js", function() {
    del(['production/compress/js/main.js', 'production/compress/js/main.min.js'])
    gulp.src("app/js/compressed.js")
        .pipe(include({
            hardFail: true,
            includePaths: [
                "production/compress/.temp"
            ]
        }))
        .pipe(minifyJs())
        .pipe(rename({
            basename: "compressed",
            extname: ".js"
        }))
        .pipe(gulp.dest("production/compress/js"));
});

gulp.task('build-js-export', function (cb) {
    return gulpSequence(
        'js-export',
        'compress-js',
        cb
    );
});


// _______________________________BUILD FONTS_________________________________



gulp.task('fonts-build', function () {
    return gulp.src(['app/fonts/**/*'])
        .pipe(gulp.dest('dist/fonts'));
});

// _______________________________BUILD IMAGES________________________________

gulp.task('images-build', function(cb) {
    gulp.src(['app/images/**/*'])
        .pipe(gulp.dest('dist/images')).on('end', cb).on('error', cb);
});

gulp.task('images-export', function(cb) {
    gulp.src(['app/images/**/*'])
        .pipe(gulp.dest('production/release/images'))
        .pipe(gulp.dest('production/compress/images')).on('end', cb).on('error', cb);
});

// _______________________________BUILD INCLUDE________________________________

gulp.task('includes-build', function () {
    gulp.src('app/includes/*')
        .pipe(gulp.dest('dist/includes'));
});

gulp.task('includes-export', function () {
    gulp.src('app/includes/*')
        .pipe(gulp.dest('production/release/includes'))
        .pipe(gulp.dest('production/compress/includes'));
});


// _______________________________WATCH FILES__________________________________

gulp.task('watch', function () {

    // Watch .pug files
    gulp.watch(
        ['app/*.html'],
        ['build-html', browserSync.reload]
    );

    // Watch .scss files
    gulp.watch(['app/scss/**/*'], ['css-build', browserSync.reload]);

    // Watch .js files
    gulp.watch('app/js/**/*.js', ['js-build', browserSync.reload]);

    // Watch image files
    gulp.watch(['app/images/**/*', 'app/images/icons/*'], ['images-build', browserSync.reload]);

});

// _______________________________MAIN TASKS___________________________________

gulp.task('build-dev', function (cb) {
    return gulpSequence(
        'vendor', 'css-build', 'fonts-build', 'images-build', 'js-build', 'build-html',
        cb
    );
});

gulp.task('build-export', function (cb) {
    return gulpSequence(
        'vendor-export', 'vendor-special-export',
        'fonts-export', 'images-export', 'build-css-export', 'build-js-export',
        'build-html-export', 'clean-temp',
        cb
    );
});

gulp.task('default',function (cb) {
    return gulpSequence(
        'clean',
        'build-dev',
        'watch',
        'browser-sync',
        cb
    );
});

gulp.task('build',function (cb) {
    return gulpSequence(
        'clean-production',
        'build-export',
        cb
    );
});

gulp.task('check',function (cb) {
    return gulpSequence(
        'watch',
        'browser-sync',
        cb
    );
});