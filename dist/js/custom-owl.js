(function($){

    "use strict";
    // use strict

    $('.au-princing-tables .owl-carousel').owlCarousel({
        items : 3,
        autoPlay: true,
        nav: false,
        dots : false,
        speed : 300,
        responsive : {
            0 : {
                items : 1,   
            },
            541 : {
                items : 2,
            },
            992: {
                items : 3
            }
        }
    });
    $('.au-works-step .owl-carousel').owlCarousel({
        items : 3,
        autoPlay: true,
        nav: false,
        dots : false,
        speed : 300,
        responsive : {
            0 : {
                items : 1,   
            },
            541 : {
                items : 2,
            },
            992: {
                items : 3
            }
        }
    });
    
    $('.au-employers-slider .owl-carousel').owlCarousel({
        items : 1,
        autoPlay: true,
        nav: true,
        loop: true,
        dots : false,
        speed : 300,
        autoHeight: true,
        navText: ['<i class="ion-android-arrow-back"><i>', '<i class="ion-android-arrow-forward"></i>'],
    });

    $('.au-jobs-carousel .owl-carousel').owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        autoHeight: true,
    });
})(jQuery);