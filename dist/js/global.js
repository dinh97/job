// Load
(function ($) {
    "use strict";
    // use strict

    $(window).on('load', function () {
        $('#preview-area').fadeOut('slow', function () {
            $(this).remove();
        });
    });
})(jQuery);

// Slick
(function ($) {
    "use strict";
    // use strict

    $('.slick-slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 200,
        arrows: false,
        fade: true,
        centerMode: true,
        rtl: $('body').hasClass('rtl') ? true : false,
        asNavFor: '.slick-slider-nav'
    });
    $('.slick-slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        speed: 200,
        asNavFor: '.slick-slider-for',
        dots: false,
        arrows: false,
        rtl: $('body').hasClass('rtl') ? true : false,
        centerMode: true,
        focusOnSelect: true,
        responsive: [{
                breakpoint: 1199,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    centerPadding: '0'
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerPadding: '0'
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerPadding: '0',
                    variableWidth: false
                }
            },
            {
                breakpoint: 479,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerPadding: '0',
                    variableWidth: false
                }
            }
        ]
    });
})(jQuery);

// Off canvas
(function ($) {
    "use strict";
    // use strict

    $(".canvas-menu").navgoco({
        caretHtml: '<span class="icon-arrow"></span>',
        accordion: false,
        openClass: 'open',
        save: true,
        cookie: {
            name: 'navgoco',
            expires: false,
            path: '/'
        },
        slide: {
            duration: 300,
            easing: 'swing'
        }
    });

    var container = $('body');
    $('.off-canvas-btn, #off-canvas-close, .au-overlay').on('click', function () {
        if (container.hasClass('canvas-menu-open')) {
            container.removeClass('canvas-menu-open');
        } else {
            container.addClass('canvas-menu-open');
        }
    });
})(jQuery);

// background
(function ($) {
    "use strict";
    // use strict

    $('.js-background').each(function () {
        var bg = $(this);
        var image = (bg.data('image') != null) ? 'url(' + bg.data('image') + ')' : null;
        var position = (bg.data('position') != null) ? bg.data('position') : 'center';
        var size = (bg.data('size') != null) ? bg.data('size') : 'cover';
        var attachment = bg.data('attachment');
        var repeat = (bg.data('repeat') != null) ? bg.data('repeat') : 'no-repeat';
        var color = bg.data('color');

        $(this).css({
            "background-color": color,
            "background-image": image,
            "background-size": size,
            "background-attachment": attachment,
            "background-repeat": repeat,
            "background-position": position
        })
    });
})(jQuery);

// Counter Up
(function ($) {
    "use strict";
    // use strict

    $('.js-counterup').each(function () {
        $(this).counterUp();
    });
})(jQuery);

// Owl Carousel
(function ($) {
    "use strcit";
    // use strict

    $(".owl-carousel").each(function () {
        var slider = $(this);
        var defaults = {
            direction: $('body').hasClass('rtl') ? 'rtl' : 'ltr'
        };
        var config = $.extend({}, defaults, slider.data("plugin-options"));
        // Initialize Slider
        slider.owlCarousel(config).addClass("owl-carousel-init");
    });

    $('.js-owl-button').each(function () {
        var next = $(this).find('.owl-next');
    });
})(jQuery);

// register-login hover 
(function ($) {
    "use strict";
    // use strict

    $('.register-login .login').on('mouseenter', function () {
        $('.register-login').addClass('login-active');
    }).on('mouseleave', function () {
        $('.register-login').removeClass('login-active');
    });
})(jQuery);

// show header search box
(function ($) {
    "use strict";
    // use strict

    $('.search-form-header span.icon').on('click', function () {
        $(this).parents('.au-header-search').toggleClass('show-search-box');
    });
})(jQuery);

// sticky header 
(function ($) {
    "use strict";
    // use strict

    var $header = $('.js-sticky-header');
    $header.data('sticky', '');
    $(window).on('scroll load resize', function () {
        var fromTop = $(document).scrollTop();
        if (fromTop > 50) {
            if ($header.data('sticky') == '') {
                $header.data('sticky', 'true');
                $header.addClass("header-sticky");
            }
        } else {
            if ($header.data('sticky') == 'true') {
                $header.data('sticky', '');
                $header.removeClass("header-sticky");
            }
        }
    });
})(jQuery);

// scroll 
(function ($) {
    "use strict";
    // use strict
    $('.js-scroll').on('click', function () {
        var href = $(this).attr('href');
        var element = $(href);
        if (element.length) {
            $("html, body").animate({
                scrollTop: element.position().top
            }, 1000);

            return false;
        }
    });
})(jQuery);

// init
(function($){
    "use strict";
    // use strict
    
    $('.au-tabs').auTabs();
})(jQuery);