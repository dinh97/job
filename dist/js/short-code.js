(function ($) {
    "use strict";
    /**
     * Tabs
     */
    $.fn.auTabs = function () {
        $(this).each(function () {
            var auTabObj = this,
                $auTab = $(this);
            var type = $auTab.data('type');
            if (type === 'tab') {
                auTabObj.content_list = $auTab.find('.au-tab-content .au-tab-item-content');
                auTabObj.list = $auTab.find('.au-tab-items .au-tab-item');
                auTabObj.item_click_index = 0;
                $('.au-tab-items .au-tab-item', this).click(function () {
                    if ($(this).hasClass('active')) {
                        return;
                    }
                    var itemclick = this,
                        item_active = $auTab.find('.au-tab-items .au-tab-item.active');
                    auTabObj.item_click_index = auTabObj.list.index(itemclick);
                    $(itemclick).addClass('active');
                    auTabObj.list.each(function () {
                        if (auTabObj.list.index(this) !== auTabObj.list.index(itemclick) && $(this).hasClass('active')) {
                            $(this).removeClass('active');
                        }
                    });
                    auTabObj.loadTabContent();
                });
                this.loadTabContent = function () {
                    var item_click = $(auTabObj.content_list.get(auTabObj.item_click_index));
                    auTabObj.content_list.each(function () {
                        if (auTabObj.content_list.index(this) < auTabObj.content_list.index(item_click)) {
                            $(this).addClass('prev').removeClass('active next');
                        } else if (auTabObj.content_list.index(this) === auTabObj.content_list.index(item_click)) {
                            $(this).addClass('active').removeClass('prev next');
                            //                            $(".map-contain",this).auMap();
                        } else {
                            $(this).addClass('next').removeClass('prev active');
                        }
                    });
                };
            } else {
                this.accordion_list = $auTab.find('.au-accordion-item');
                $('.au-accordion-header', this).click(function () {
                    var itemClick = $(this);
                    var item_target = itemClick.parent();
                    if (itemClick.hasClass('active')) {
                        itemClick.removeClass('active');
                        item_target.find('.au-accordion-content').slideUp({
                            easing: 'easeOutQuad'
                        });
                        item_target.find('.au-accordion-header-icon .expand').hide();
                        item_target.find('.au-accordion-header-icon .no-expand').show();
                        return;
                    }
                    itemClick.addClass('active');
                    item_target.find('.au-accordion-content').slideDown({
                        easing: 'easeOutQuad'
                    });
                    item_target.find('.au-accordion-header-icon .expand').show();
                    item_target.find('.au-accordion-header-icon .no-expand').hide();
                    auTabObj.accordion_list.each(function () {
                        if (auTabObj.accordion_list.index(this) !== auTabObj.accordion_list.index(item_target) && $(this).find('.au-accordion-header').hasClass('active')) {
                            $(this).find('.au-accordion-header').removeClass('active');
                            $(this).find('.au-accordion-content').slideUp({
                                easing: 'easeOutQuad'
                            });
                            $(this).find('.au-accordion-header-icon .expand').hide();
                            $(this).find('.au-accordion-header-icon .no-expand').show();
                        }
                    });
                });

                $('.au-accordion-header', this).hover(function () {
                    var item = $(this),
                        item_target = item.parent();
                    if (item.hasClass('active')) {
                        return;
                    }
                    item_target.find('.au-accordion-header-icon .expand').show();
                    item_target.find('.au-accordion-header-icon .no-expand').hide();
                }, function () {
                    var item = $(this),
                        item_target = item.parent();
                    if (item.hasClass('active')) {
                        return;
                    }
                    item_target.find('.au-accordion-header-icon .expand').hide();
                    item_target.find('.au-accordion-header-icon .no-expand').show();
                });
            }

        });
    };
})(jQuery);